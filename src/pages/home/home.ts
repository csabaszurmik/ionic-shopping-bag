import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ShoppingBagPage } from '../shoppingbag/shoppingbag';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	shoppingBagPage = ShoppingBagPage;
	bags: any = [];
  constructor(
  	public navCtrl: NavController,
		public storage: Storage
  	) {
  	// this.updateBagList();
  }


  ionViewWillEnter(){
    this.updateBagList();
  }

  updateBagList() {
  	this.bags = [];
  	let bags_raw = [];
  	this.storage.keys().then((prom) => {
	    console.log('keys', prom);
	    for (let i=0;i<prom.length;i++) {
	    	if (prom[i].lastIndexOf('bag_', 0) === 0) {
	    		this.storage.get(prom[i]).then((val) => {
				    bags_raw.push(JSON.parse(val));
					  this.bags = bags_raw.sort(function(b,a){
					  	console.log(a,b);
				     	return a.updated > b.updated ? 1:a.updated < b.updated?-1:0
				    })
				  });
	    	}
	    }
    	console.log('this.bags',this.bags);
	  });	
  	
  }

  open(bag) {
		let id = 'new';
  	if (bag) {
  		id = bag.id
  	}
		this.navCtrl.push(ShoppingBagPage,{id: id});
  }

  clear() {
  	this.storage.clear();
  	this.bags = [];
  }



}
