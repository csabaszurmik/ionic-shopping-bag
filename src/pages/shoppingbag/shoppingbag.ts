import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-shoppingbag',
  templateUrl: 'shoppingbag.html'
})
export class ShoppingBagPage {
	editing: boolean = true;
	current_item: any = {};
	items: any = [];
	archives: any = [];
	matches: any = [];
	qty_options: any = [];
	add_btn_disabled:boolean=false;
	db_key: string = '';
	current_date: any = '';
	created: any = '';
	updated: any = '';
	title: string = '';
	id: string = '';
	
  constructor(
		public navCtrl: NavController, 
    public navParams: NavParams,
		public alertCtrl: AlertController,
		public storage: Storage
		) {

  	
  	this.current_item.name = '';
  	this.current_item.qty = 1;
  	this.add_btn_disabled = true;

  	for (let i=1;i<50;i++) {
  		this.qty_options.push(i);
  	}
  	this.archives = []

  	this.storage.keys().then((prom) => {
	    console.log('keys', prom);
	    for (let i=0;i<prom.length;i++) {
	    	if (prom[i].lastIndexOf('bag_', 0) === 0) {
	    		this.storage.get(prom[i]).then((val) => {
				    let bag = JSON.parse(val);
				    for (let j=0;j<bag.items.length;j++) {
				    	if (this.archives.indexOf(bag.items[j].name)==-1) this.archives.push(bag.items[j].name)
				    }
				  });
	    	}
	    }
	  });	

  	this.id = this.navParams.get('id');
  	if (this.id=='new') {
	  	this.current_date = new Date();
	  	this.created = this.current_date;
	  	this.db_key = 'bag_'+this.current_date.getTime();
  	} else {
  		this.db_key = this.id;
  		console.log(this.id.slice(4))
  		this.current_date = new Date(parseInt(this.id.slice(4))*1000);
  		this.storage.get(this.db_key).then((val) => {
		    let bag = JSON.parse(val);
		    console.log('current bag', JSON.parse(val));

		    this.items = bag.items;
		    this.title = bag.title;
		    this.created = bag.created;
		    
		  });
  	}
  	this.updated = this.current_date;
  	console.log('current_date',this.current_date);
  	console.log('db_key',this.db_key);

	}

  inputChange() {
  	this.matches = [];
  	if (this.current_item.name=='') {
  		this.matches = [];
  		this.add_btn_disabled = true;
  	} else {
  		this.add_btn_disabled = false;
	  	this.archives.forEach((archive) => {
	      if (archive.toLowerCase().indexOf(this.current_item.name.toLowerCase())>-1) {
	      	if (this.items.filter(e => e.name.toLowerCase() == archive.toLowerCase()).length == 0) {
					  this.matches.push(archive)
					}
				}
	   	});
  	}
  }

  matchSelect(match) {
  	this.current_item = {
				name:match,
				qty:this.current_item.qty
			}
  }
  matchAdd(match) {
  	this.current_item.name = match
  	this.add()
  }

  addDisabled() {
  	return (this.current_item.qty==0) ? true : false;
  }

  add() {
  	this.items.push(
			{
				name:this.current_item.name,
				qty:this.current_item.qty,
				done: false
			}
  	)

  	for(var i = 0; i < this.matches.length; i++) {
	    if (this.matches[i].toLowerCase() == this.current_item.name.toLowerCase()) {
        this.matches.splice(i, 1);
        break;
	    }
		}

		this.updateBag();

  	this.current_item.name = '';
  	this.current_item.qty = 1;
  	this.add_btn_disabled = true;


  }

  decrQty() {
  	if (this.current_item.qty>1) this.current_item.qty--;
  }

  incrQty() {
  	this.current_item.qty++;
  }

  editItem(item){
 
      let prompt = this.alertCtrl.create({
          title: 'Edit Item',
          inputs: [{
              type: 'text',
              name: 'name',
              value: item.name
          },
          {
              type: 'select',
              name: 'qty',
              value: item.qty
          }],
          buttons: [
              {
                  text: 'Cancel'
              },
              {
                  text: 'Save',
                  handler: data => {
                      let index = this.items.indexOf(item);
												console.log('item',item);
												console.log('data',data);
                      if(index > -1){
                        this.items[index] = data;

                      	this.updateBag();
                      }
                  }
              }
          ]
      });

      prompt.present();       

  }

  deleteItem(item){

      let index = this.items.indexOf(item);

      if(index > -1){
        this.items.splice(index, 1);
      	this.updateBag();
      }
  }

  edit() {
  	this.editing = true;
  }
  save() {
  	this.editing = false;
  	this.updateBag();
  }

  done(item) {
  	for(var i = 0; i < this.items.length; i++) {
	    if (this.items[i].name.toLowerCase() == item.name.toLowerCase()) {
        this.items[i].done = !this.items[i].done;
        break;
	    }
		}

    console.log(this.items)
    this.updateBag();

  }
  updateBag() {
  	if (this.title=='') {
		  // var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		  var year = this.current_date.getFullYear();
		  var month = this.current_date.getMonth()+1
		  var date = this.current_date.getDate();
		  // var hour = this.current_date.getHours();
		  // var min = this.current_date.getMinutes();
		  // var sec = this.current_date.getSeconds();
		  // var time = year + '/' + month + '/' + date;

  		this.title = year + '/' + month + '/' + date;
  	}

  	this.storage.set(this.db_key, JSON.stringify(
  		{
  			id: this.db_key, 
  			title: this.title, 
  			items:this.items,
  			created:this.created,
  			updated:this.updated,
  		}
  	));
    this.storage.get(this.db_key).then((val) => {
	    console.log('current bag', JSON.parse(val));
	  });
    this.storage.keys().then((prom) => {
	    console.log('keys', prom);
	  });	
  }


}
